# meet-an-immigrant

## Installing dependencies & running up
To work, the **Pixel2HTML Boilerplate** needs to install some dependencies to run the options you select.
For this job, run this command in your shell

```
$ npm run start
```

## File Structure

This boilerplate will create a set of files and folders

```

/dist
/src
   /assets
      /fonts
      /gulp
      /icons
      /images
      /js
      /styles
            /components
                  _buttons.scss
                  _footer.scss
                  _header.scss
                  _nav.scss
            /screens
                  _base.scss
                  screen_1.scss
                  screen_2.scss
                  screen_3.scss
                  screen_4.scss
                  screen_5.scss
                  screen_6.scss

                        main.scss
            mixins.scss
            variables.scss
      /vendor

    screen_1.html
    screen_2.html
    screen_3.html
    screen_4.html
    screen_5.html
    screen_6.html
.bowerrc
.gitattributes
.gitignore
.jshintrc
.editorcofig
.project.conf
bower.json
gulpfile.js
package.json
```

## Available Gulp Commands

### Helpers
* `$ gulp clean` Clean /dist directory

### Static Files
* `$ gulp main:static` Compile static files (images, fonts, icons)
* `$ gulp main:static:images` Move images
* `$ gulp main:static:fonts` Move fonts
* `$ gulp main:static:icons` Move icons

### Scripts
* `$ gulp main:scripts` Concat, uglify and move JS files

### Styles
* `$ gulp main:styles` Compile, concat, autoprefix, minify and move [SCSS, Less, Stylus] files

### Vendors




### jQuery
* `$ gulp vendor:jquery` Compile & move all **jQuery** files
* `$ gulp vendor:jquery:scripts` Compile, concat, minify and move **jQuery** files

### Daemons
* `$ gulp watch` **Watch** your files and autoexecute gulp directives
* `$ gulp serve` **Watch** your files and **serve** with an HTTP server and **Sync** with your prefered browser _awesome!_

### Delivery
 * `$ gulp build` Execute all the gulp directives and makes a `.zip` file with the latest code.
