/**
 * Pixel2HTML - 4320/meet-and-immigrant
 */

$(function(){

  // $("#sticker").sticky({topSpacing:0});

  new WOW().init();

  $('.testimonial-carousel').slick({
    dots: true,
    // autoplay: true,
    // autoplaySpeed: 4000,
    speed: 900,
    arrows: false,
    cssEase: 'cubic-bezier(0.400, 0.185, 0.580, 1.000)'
  });

  // Hamburger menu

  $('.hamburguer-btn').on('click', function(){
    $(this).toggleClass('is-active');
    $('.menu-overlay').toggleClass('is-active');
    $('body').toggleClass('menu-is-open');
    $('.donate-btn').toggleClass('menu-is-open');
    $('.main-logo').toggleClass('is-hidden');
    $('.main-title').toggleClass('is-hidden');
  });

  // Sticky elements

  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= 48 && $(window).width() > 992 && $('body').hasClass('shrink') ) {
        $('.hamburguer-btn').addClass('is-scrolling');
        $('.main-logo').addClass('is-scrolling');
        $('.main-title').addClass('is-hidden');
        // $('.secondary-logo').addClass('is-fixed');
        $('.link-create-photo').addClass('is-scrolling');
        $('.link-tell-story').addClass('is-scrolling');
        $('.donate-btn').addClass('is-scrolling');
      } else {
        $('.hamburguer-btn').removeClass('is-scrolling');
        $('.main-logo').removeClass('is-scrolling');
        $('.main-title').removeClass('is-hidden');
        // $('.secondary-logo').removeClass('is-fixed');
        $('.link-create-photo').removeClass('is-scrolling');
        $('.link-tell-story').removeClass('is-scrolling');
        $('.donate-btn').removeClass('is-scrolling');
      }
  });

    // $(window).scroll(function() {
    //   var scroll = getCurrentScroll();
    //     if ( scroll >= 650 && $(window).width() > 992 ) {
    //       $('.link-share').addClass('is-fixed');
    //       $('.link-create-photo').addClass('is-fixed');
    //       $('.link-tell-story').addClass('is-fixed');
    //     } else {
    //       $('.link-share').removeClass('is-fixed');
    //       $('.link-create-photo').removeClass('is-fixed');
    //       $('.link-tell-story').removeClass('is-fixed');
    //     }
    // });

  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }

  // Load more

  $('.story-cards-col').slice(0, 3).addClass('is-active');
  $('.load-more-btn').on('click', function(){
    $(this).hide();
    $('.story-cards-col:hidden').addClass('is-active');
  });

});

// Floatl

if ($('form').length) {
  new Floatl(document.querySelectorAll('.js-floatl')[0]);
  new Floatl(document.querySelectorAll('.js-floatl')[1]);
  new Floatl(document.querySelectorAll('.js-floatl')[2]);
  new Floatl(document.querySelectorAll('.js-floatl')[3]);
}
